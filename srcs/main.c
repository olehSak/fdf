/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 21:01:48 by osak              #+#    #+#             */
/*   Updated: 2018/06/14 14:22:11 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	initialization(t_point *fdf, t_map *map, int endian)
{
	int			bpp;
	int			size_line;
	int			c;
	t_window	*window;

	c = (((map->cx - 1) * map->cy + map->cx * (map->cy - 1)) * 1);
	window = (t_window*)malloc(sizeof(t_window));
	window->m_ptr = mlx_init();
	window->w_ptr = mlx_new_window(window->m_ptr, WINW, WINH, WINNAME);
	window->mas = 0.3;
	window->disp_x = 0.5;
	window->disp_y = 0.5;
	window->img = mlx_new_image(window->m_ptr, WINW, WINH);
	window->data =
			(int *)mlx_get_data_addr(window->img, &bpp, &size_line, &endian);
	window->model.normalized_points = fdf;
	window->model.points_count = map->su;
	window->model.transformed_points =
			(t_point*)malloc(sizeof(t_point) * window->model.points_count);
	window->model.count_points_x = map->cx;
	window->model.count_points_y = map->cy;
	window->model.line = (t_line *)malloc(sizeof(t_line) * c);
	mlx_hook(window->w_ptr, 2, 3, rise_event, window);
	mlx_hook(window->w_ptr, 17, 1L << 17, rise_event, 0);
	mlx_loop(window->m_ptr);
}

void	converter_map_fdf(t_map *map, t_point *fdf, float maxi)
{
	int		x;
	int		y;
	int		a;
	int		i;

	i = -1;
	a = 0;
	x = 0;
	y = map->cy;
	while (--y >= 0 && ++i < map->cy)
	{
		x = map->cx;
		while (--x >= 0)
		{
			fdf[a].z = (map->points[i][x] * maxi);
			fdf[a].x = (x * maxi) - 1;
			fdf[a].y = (y * maxi) - 0.5f;
			a++;
		}
	}
}

int		main(int argc, char **argv)
{
	t_map	*map;
	int		c_v;
	int		c_h;
	float	maxi;
	t_point	*fdf;

	c_v = 0;
	c_h = 0;
	if (argc != 2)
	{
		ft_putstr("Only one argument needed.");
		exit(1);
	}
	map = read_file(argv, c_v, c_h);//leak 2
	maxi = scaling(map);
	map->su = map->cx * map->cy;
	fdf = (t_point*)malloc(sizeof(t_point) * map->su);
	converter_map_fdf(map, fdf, maxi);
	initialization(fdf, map, 1);
	return (0);
}
