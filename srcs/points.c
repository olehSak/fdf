/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   points.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 17:55:49 by osak              #+#    #+#             */
/*   Updated: 2018/06/13 17:02:36 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

float	scaling(t_map *map)
{
	int			x;
	int			y;
	long int	tx;
	long int	ty;
	long int	tz;

	tx = 0;
	ty = 0;
	tz = 0;
	y = map->cy;
	while (--y >= 0)
	{
		x = map->cx;
		while (--x >= 0)
		{
			(x > tx) ? (tx = x) : 0;
			(y > ty) ? (ty = y) : 0;
			(tz < map->points[y][x]) ? (tz = map->points[y][x]) : 0;
		}
	}
	tx < ty ? (tx = ty) : (tx);
	tz < tx ? (tx) : (tx = tz);
	return ((float)2 / tx);
}

void	convert(t_model model, int *data)
{
	t_point	*point;
	int		i;
	int		z;
	int		x;
	int		y;

	point = model.transformed_points;
	x = model.count_points_x;
	y = model.count_points_y;
	z = 1;
	i = -1;
	while (++i < x * y * z)
	{
		if ((i + 1) % x != 0)
		{
			*model.line = point_line(point[i], point[i + 1], WHITE, data);
			model.line++;
		}
		if (i < (x * y - x))
		{
			*model.line = point_line(point[i], point[i + x], WHITE, data);
			model.line++;
		}
	}
}
