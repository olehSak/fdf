/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 14:19:10 by osak              #+#    #+#             */
/*   Updated: 2018/06/13 17:32:37 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_isspace(char *c)
{
	int		it;

	it = 0;
	while (c[it] < 33)
		it++;
	it = 0;
	while (c[it] != 0)
	{
		if (ft_isalpha(c[it]))
		{
			ft_putstr("CHAR IN MAP");
			exit(1);
		}
		it++;
	}
	return (ft_atoi(c));
}

t_map	*map_int(int fd, char *str, long int **istr)
{
	t_map	*map;
	int		c_v;
	int		c_h;
	char	**sstr;

	map = (t_map*)malloc(sizeof(t_map));
	c_v = 0;
	c_h = 0;
	while (get_next_line(fd, &str))
	{
		c_h = 0;
		sstr = ft_strsplit(str, ' ');
		while (sstr[c_h])
		{
			istr[c_v][c_h] = ft_isspace(sstr[c_h]);
			free(sstr[c_h]);
			c_h++;
		}
		free(sstr);
		free(str);
		c_v++;
	}
	free(str);
	map->cx = c_h;
	map->cy = c_v;
	map->points = istr;
	return (map);
}

t_map	*create_map(int c_v, int c_h, char **argv, char *str)
{
	int			fd;
	long int	**istr;

	istr = (long int **)malloc(sizeof(long int *) * c_v);
	while (--c_v > -1)
	{
		istr[c_v] = (long int *)malloc(sizeof(long int) * c_h);
	}
	if ((fd = open(argv[1], O_RDONLY)) < 0)
	{
		ft_putstr("Cannot open file.\n");
		exit(1);
	}
	return (map_int(fd, str, istr));//leak
}

void	error_handler(int c_h, int c_v, int all_hor_pts)
{
	if (c_h == 0)
	{
		ft_putstr("CLEAR MAP\n");
		exit(1);
	}
	if (all_hor_pts % c_v > 0 || all_hor_pts % c_h > 0)
	{
		ft_putstr("ERROR MAP\n");
		exit(1);
	}
}

t_map	*read_file(char **argv, int c_v, int c_h)
{
	char		*str;
	char		**easter;
	int			all_hor_pts;
	int			fd;

	all_hor_pts = 0;
	if ((fd = open(argv[1], O_RDONLY)) < 0)
	{
		ft_putstr("Cannot open file.\n");
		exit(1);
	}
	while (get_next_line(fd, &str))
	{
		c_h = -1;
		easter = ft_strsplit(str, ' ');
		while (easter[++c_h])
		{
			all_hor_pts++;
			free(easter[c_h]);
		}
		free(easter);
        free(str);
        str = 0;
		c_v++;
	}
	free(str);
	error_handler(c_h, c_v, all_hor_pts);
	close(fd);
	return (create_map(c_v, c_h, argv, str));
}
