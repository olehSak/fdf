/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   system.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 21:02:07 by osak              #+#    #+#             */
/*   Updated: 2018/06/14 15:01:29 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	rotation(t_window *window, t_point xyzw)
{
	t_model		*model;
	float		mator[16];
	float		angle;
	int			i;

	angle = 30.f / 180.f * 3.14f;
	xyzw.w = cosf(angle * 0.5f);
	model = &(window->model);
	mlx_clear_window(window->m_ptr, window->w_ptr);
	ft_memset(window->data, 0, WINW * WINH * 4);
	ft_memset(mator, 0, 16 * sizeof(float));
	matrix(mator, xyzw);
	i = -1;
	while (++i < model->points_count)
		model->normalized_points[i] =
				ft_matvec(model->normalized_points[i], mator);
	i = -1;
	while (++i < model->points_count)
		model->transformed_points[i] =
				changer(model->normalized_points[i], *window);
	convert(*model, window->data);
	mlx_put_image_to_window(window->m_ptr, window->w_ptr, window->img, 0, 0);
}

int		rise_event(int key, void *par)
{
	t_window	*window;
	t_point		xyzw;

	window = (t_window*)par;
	xyzw.x = 0;
	xyzw.y = 0;
	xyzw.z = 0;
	if (key == 53 || !key)
		exit(1);
	key == 24 ? (window->mas += 0.1) : 0;
	key == 27 ? (window->mas -= 0.1) : 0;
	key == 123 ? (xyzw.y = 0.1f) : 0;
	key == 125 ? (xyzw.x = 0.1f) : 0;
	key == 124 ? (xyzw.y = -0.1f) : 0;
	key == 126 ? (xyzw.x = -0.1f) : 0;
	key == 88 ? (window->disp_x += 0.1) : 0;
	key == 86 ? (window->disp_x -= 0.1) : 0;
	key == 91 ? (window->disp_y += 0.1) : 0;
	key == 84 ? (window->disp_y -= 0.1) : 0;
	rotation(window, xyzw);
	return (0);
}
