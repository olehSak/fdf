/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/14 16:11:47 by osak              #+#    #+#             */
/*   Updated: 2018/06/14 16:14:27 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_V0_HEADER_H
# define FDF_V0_HEADER_H

# define ABS(x) (x < 0) ? -(x) : (x)
# define WHITE 0xffffff
# define RED 0xff0101
# define WINW 1200
# define WINH 800
# define WINNAME "windows"

# include <time.h>
# include <stdlib.h>
# include "get_next_line.h"
# include "libft.h"
# include "mlx.h"
# include "math.h"

typedef struct	s_point
{
	float		x;
	float		y;
	float		z;
	float		w;
}				t_point;

typedef struct	s_line
{
	int			x0;
	int			y0;
	int			x1;
	int			y1;
	int			color;
	int			*data;
}				t_line;

typedef struct	s_map
{
	int			cx;
	int			cy;
	int			su;
	long int	**points;
}				t_map;

typedef struct	s_model
{
	t_point		*normalized_points;
	t_point		*transformed_points;
	int			points_count;
	t_line		*line;
	int			count_points_x;
	int			count_points_y;
}				t_model;

typedef struct	s_window
{
	void		*m_ptr;
	void		*w_ptr;
	void		*img;
	int			*data;
	t_model		model;
	float		mas;
	float		disp_y;
	float		disp_x;
}				t_window;

int				matrix(float *matrixofrotation, t_point xyzw);
t_point			changer(t_point points, t_window wind);
t_point			ft_matvec(t_point p, float *m);
t_map			*read_file(char **argv, int c_v, int c_h);
void			segment(t_line line);
t_line			point_line(t_point startp, t_point endp, int clr, int *data);
void			convert(t_model model, int *data);
float			scaling(t_map *map);
int				rise_event(int key, void *par);
void			rotation(t_window *window, t_point xyzw);

#endif
