NAME = fdf

CC = gcc

CFLAGS = -Wall -Wextra -Werror

HEADERS = headers/

SRCS = main.c\
       changer.c\
       get_next_line.c\
       points.c\
       read_file.c\
       system.c\

OBJ = $(SRCS:.c=.o)

LIBFTDIR = libft/

MINILIBXDIR = minilibx_macos/

all: $(NAME)

$(NAME)	:	$(OBJ) $(LIBFTDIR)libft.a
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L./$(LIBFTDIR) -lft

$(NAME)	:	$(OBJ) $(MINILIBXDIR)libmlx.a
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L./$(MINILIBXDIR) -lmlx

%.o: srcs/%.c $(HEADERS)
	$(CC) $(CFLAGS) -c -o $@ $< -I $(HEADERS) -I $(LIBFTDIR)

$(LIBFTDIR)libft.a: libft/Makefile
	make -C libft/

$(MINILIBXDIR)libmlx.a: minilibx_macos/Makefile
	make -C minilibx_macos/

clean:
	rm -f $(OBJ)
	make clean -C $(LIBFTDIR) 

fclean: clean
	rm -f $(NAME)
	make fclean -C $(LIBFTDIR) 

re:  all
	make re -C $(LIBFTDIR)

norm:
	norminette *.c srcs/*.c headers/*.h
	make norm -C $(LIBFTDIR)

rmsh:
	find . -name ".DS*" -o -name "._.*" -o -name "#*" -o -name "*~" -o -name "*.out" > rmsh
	xargs rm < rmsh
	rm rmsh
